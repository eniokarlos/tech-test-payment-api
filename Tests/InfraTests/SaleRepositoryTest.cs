using TechTestPaymentApi.Domain.Entities;
using TechTestPaymentApi.Infraestructure.Context;
using TechTestPaymentApi.Infraestructure.Repositories;

namespace TechTestPaymentApi.Tests.InfraTests;

public class SaleRepositoryTest
{
    [Fact]
    public void GivenAValidSale_WhenRegisterASale_ThenReturnTheSaleRegistered()
    {
        //Given
        var expectedSale = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        var repository = new SaleRepository();

        //When
        var actualSale = repository.Register(expectedSale);

        //Then
        Assert.Equal(expectedSale, actualSale);
        Assert.Equal(expectedSale, InMemoryDataBase.Sales[expectedSale.Id]);
    }

    [Fact]
    public void GivenAValidSale_WhenUpdateASale_ThenReturnTheSaleUpdated()
    {
        //Given
        var expectedSale = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        var repository = new SaleRepository();

        //When
        repository.Register(expectedSale);
        var actualSale = repository.UpdateStatus(expectedSale.Id,Sale.Status.PAYMENT_APPROVED);

        //Then
        Assert.Equal(expectedSale, actualSale);
        Assert.Equal(expectedSale.SaleStatus, actualSale.SaleStatus);
        Assert.Equal(expectedSale, InMemoryDataBase.Sales[expectedSale.Id]);
    }

    [Fact]
    public void GivenAValidSale_WhenGetASaleById_ThenReturnASale()
    {
        //Given
        var expectedSale = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        var repository = new SaleRepository();

        //When
        repository.Register(expectedSale);
        var actualSale = repository.GetById(expectedSale.Id);

        //Then
        Assert.Equal(expectedSale, actualSale);
        Assert.Equal(expectedSale, InMemoryDataBase.Sales[expectedSale.Id]);
    }

    [Fact]
    public void GivenValidSales_WhenGetSales_ThenReturnAListOfSales()
    {
        //Given
        var expectedSale1 = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        var expectedSale2 = new Sale(
            new Seller(
                "Mary",
                "135.356.697-10",
                "Mary@gmail.com",
                "(88) 96967 9597"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Book", 17.865m, 2),
            }
        );

        var repository = new SaleRepository();
        repository.Register(expectedSale1);
        repository.Register(expectedSale2);

        //When
        var sales = repository.GetSales();

        //Then
        Assert.Contains<Sale>(expectedSale1, sales);
        Assert.Contains<Sale>(expectedSale2, sales);
    }
}