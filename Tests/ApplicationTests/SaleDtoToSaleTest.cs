using TechTestPaymentApi.Application.DTOs;
using TechTestPaymentApi.Application.Mappings;
using TechTestPaymentApi.Domain.Entities;

namespace TechTestPaymentApi.Tests.ApplicationTests;

public class SaleDtoToSaleTest
{
    [Fact]
    public void GivenASaleInDto_WhenMapSaleInDtoToSale_ThenReturnAValidSale()
    {
        //Given
        var expectedSale = new SaleIn()
        {
            Seller = new SellerIn()
            {
                Name = "John",
                Cpf = "124.567.768-00",
                Email = "John@gmail.com",
                Phone = "(88) 94657-0496"
            },

            Items = new List<SaleItemIn>()
            {
                new SaleItemIn()
                {
                    Name = "example",
                    Quantity = 1,
                    unitPrice = 10m
                }
            }
        };

        //When
        var config = new MapperConfiguration(cfg =>
            cfg.AddProfile(new DomainToDtoMapping()));
        
        var mapper = new Mapper(config);
        var actualSale = mapper.Map<Sale>(expectedSale);

        //Then
        Assert.NotNull(actualSale);
        Assert.NotNull(actualSale.Seller);
        Assert.NotNull(actualSale.Items);
    }

    [Fact]
    public void GivenASale_WhenMapSaleToSaleOutDto_ThenReturnAValidSaleOut()
    {
        //Given
        var expectedSale = new Sale(
            new Seller(      
                "John",
                "124.567.768-00",
                "John@gmail.com",
                "(88) 94657-0496"
            ),

            new List<SaleItem>() 
            {     
                new SaleItem(             
                    "example",
                    10m,
                    1
                )
            }
        );

        //When
        var config = new MapperConfiguration(cfg =>
            cfg.AddProfile(new DomainToDtoMapping()));
        
        var mapper = new Mapper(config);
        var actualSale = mapper.Map<SaleOut>(expectedSale);

        //Then
        Assert.NotNull(actualSale);
        Assert.NotNull(actualSale.Id);
        Assert.Equal(expectedSale.Date, actualSale.Date);
        Assert.Equal(expectedSale.SaleStatus, actualSale.SaleStatus);
        Assert.Equal(expectedSale.GetTotal().Amount, actualSale.Total);
        Assert.NotNull(actualSale.Seller);
        Assert.NotNull(actualSale.Items);
    }
}
