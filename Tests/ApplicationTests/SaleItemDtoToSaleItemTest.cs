using TechTestPaymentApi.Application.DTOs;
using TechTestPaymentApi.Application.Mappings;
using TechTestPaymentApi.Domain.Entities;

namespace TechTestPaymentApi.Tests.ApplicationTests;

public class SaleItemDtoToSaleItemTest
{
    [Fact]
    public void GivenAValidSaleItemInDto_WhenMapSaleItemInDtoToSaleItem_ThenReturnASaleItem()
    {
        //Given
        var expectedSaleItem = new SaleItemIn()
        {
            Name = "Example",
            unitPrice = 10.76m,
            Quantity = 2
        };

        //When
        var config = new MapperConfiguration(cfg =>
            cfg.AddProfile(new DomainToDtoMapping()));
        
        var mapper = new Mapper(config);
        var actualSaleItem = mapper.Map<SaleItem>(expectedSaleItem);

        //Then
        Assert.NotNull(actualSaleItem);
        Assert.Equal(expectedSaleItem.Name, actualSaleItem.Name);
        Assert.Equal(expectedSaleItem.Quantity, actualSaleItem.Quantity);
        Assert.Equal(expectedSaleItem.unitPrice, actualSaleItem.UnitPrice.Amount);
    }

    [Fact]
    public void GivenAValidSaleItem_WhenMapSaleItemToSaleItemOutDto_ThenReturnASaleItemOutDto()
    {
        //Given
        var expectedSaleItem = new SaleItem(
            "Example",
            10.76m,
            2
        );
    

        //When
        var config = new MapperConfiguration(cfg =>
            cfg.AddProfile(new DomainToDtoMapping()));
        
        var mapper = new Mapper(config);
        var actualSaleItem = mapper.Map<SaleItemOut>(expectedSaleItem);

        //Then
        Assert.NotNull(actualSaleItem);
        Assert.Equal(expectedSaleItem.Name, actualSaleItem.Name);
        Assert.Equal(expectedSaleItem.Quantity, actualSaleItem.Quantity);
        Assert.Equal(expectedSaleItem.UnitPrice.Amount, actualSaleItem.UnitPrice);
    }
}