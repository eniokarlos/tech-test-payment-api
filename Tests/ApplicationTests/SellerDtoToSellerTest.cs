using TechTestPaymentApi.Application.DTOs;
using TechTestPaymentApi.Application.Mappings;
using TechTestPaymentApi.Domain.Entities;

namespace TechTestPaymentApi.Tests.ApplicationTests;

public class SellerDtoToSellerTest
{
    [Fact]
    public void GivenAValidSellerInDto_WhenMapSellerInDtoToSeller_ThenReturnAValidSeller()
    {
        //Given
        var expectedSeller = new SellerIn()
        {
            Name = "John",
            Cpf = "123.456.678-00",
            Email = "John@email.com",
            Phone = "(00)0 0000-0000"
        };

        //When
        var config = new MapperConfiguration(cfg =>
            cfg.AddProfile(new DomainToDtoMapping()));
        
        var mapper = new Mapper(config);
        var actualSeller = mapper.Map<Seller>(expectedSeller);

        //Then
        Assert.NotNull(actualSeller);
        Assert.Equal(expectedSeller.Name, actualSeller.Name);
        Assert.Equal(expectedSeller.Email, actualSeller.Email);
        Assert.Equal(expectedSeller.Cpf, actualSeller.Cpf.Value);
        Assert.Equal(expectedSeller.Phone, actualSeller.Phone);
    }

    [Fact]
    public void GivenAValidSeller_WhenMapSellerToSellerOutDto_ThenReturnAValidSellerOutDto()
    {
        //Given
        var expectedSeller = new Seller(
            "John",
            "123.456.789-00",
            "John@email.com",
            "(00)00000 0000"
        );

        //When
        var config = new MapperConfiguration(cfg =>
            cfg.AddProfile(new DomainToDtoMapping()));
        
        var mapper = new Mapper(config);
        var actualSeller = mapper.Map<SellerOut>(expectedSeller);

        //Then
        Assert.NotNull(actualSeller);
        Assert.Equal(expectedSeller.Name, actualSeller.Name);
        Assert.Equal(expectedSeller.Email, actualSeller.Email);
        Assert.Equal(expectedSeller.Cpf.Value, actualSeller.Cpf);
        Assert.Equal(expectedSeller.Phone, actualSeller.Phone);
    }
}