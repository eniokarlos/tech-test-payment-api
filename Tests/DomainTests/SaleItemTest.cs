using TechTestPaymentApi.Domain.Entities;
using TechTestPaymentApi.Domain.Validation;

namespace TechTestPaymentApi.Tests.DomainTests;

public class SaleItemTest
{
[Fact]
public void GivenValidParams_WhenCreateASaleItem_ThenReturnASaleItemCreated()
{
    // Given
    var expectedName = "Example";
    var expectedQuantity = 10;
    var expectedUnitPrice = 50.0m;
    var expectedTotalPrice = 500.0m;

    // When
    var actualSaleItem = new SaleItem(
        "Example",
        50.0m,
        10
    );
    // Then
    Assert.NotNull(actualSaleItem);
    Assert.NotNull(actualSaleItem.Id);
    Assert.Equal(expectedName, actualSaleItem.Name);
    Assert.Equal(expectedQuantity, actualSaleItem.Quantity);
    Assert.Equal(expectedUnitPrice, actualSaleItem.UnitPrice.Amount);
    Assert.Equal(expectedTotalPrice, actualSaleItem.GetTotalPrice());
}

    [Theory]
    [InlineData("")]
    [InlineData(null)]
    public void GivenInvalidNullOrEmptyName_WhenCreateASaleItem_ThenThrowException(string name)
    {
        //Given
        var expectedErrorMessage = "'Name' should not be null or empty";

        //When
        var actualException = Assert.Throws<DomainExceptionValidation>(()=>
            new SaleItem(
                name,
                50.0m,
                10
            )
        );

        //Then
        Assert.Equal(expectedErrorMessage, actualException.Message);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-1)]
    public void GivenInvalidZeroOrNegativeQuantity_WhenCreateASaleItem_ThenThrowException(int quantity)
    {
        //Given
        var expectedErrorMessage = "'Quantity' must be greater than zero";

        //When
        var actualException = Assert.Throws<DomainExceptionValidation>(()=>
            new SaleItem(
                "Name",
                50.0m,
                quantity
            )
        );

        //Then
        Assert.Equal(expectedErrorMessage, actualException.Message);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-1)]
    public void GivenInvalidZeroOrNegativePrice_WhenCreateASaleItem_ThenThrowException(decimal price)
    {
        //Given
        var expectedErrorMessage = "'Price' must be greater than zero";

        //When
        var actualException = Assert.Throws<DomainExceptionValidation>(()=>
            new SaleItem(
                "Name",
                price,
                10
            )
        );

        //Then
        Assert.Equal(expectedErrorMessage, actualException.Message);
    }
}