using TechTestPaymentApi.Domain.ValueObjects;

namespace TechTestPaymentApi.Tests.DomainTests;

public class MoneyTest
{
    [Fact]
    public void GivenAnyValidParams_WhenCreateAMoney_ThenReturnAMoneyCreated()
    {
        // Given
        decimal decimalAmount = 10.255m;
        string stringAmountWithPoint = "10.255";
        string stringAmountWithComma = "10,255";
        double doubleAmount = 10.255;
        decimal expectedDecimalAmount = 10.26m;


        // When
        var actualMoneyWithDecimal = Money.Of(decimalAmount);
        var actualMoneyWithPointString = Money.Of(stringAmountWithPoint);
        var actualMoneyWithCommaString = Money.Of(stringAmountWithComma);
        var actualMoneyWithDouble = Money.Of(doubleAmount);

        // Then
        Assert.NotNull(actualMoneyWithDecimal);
        Assert.NotNull(actualMoneyWithPointString);
        Assert.NotNull(actualMoneyWithCommaString);
        Assert.NotNull(actualMoneyWithDouble);
        Assert.Equal(expectedDecimalAmount, actualMoneyWithDecimal.Amount);
        Assert.Equal(expectedDecimalAmount, actualMoneyWithPointString.Amount);
        Assert.Equal(expectedDecimalAmount, actualMoneyWithCommaString.Amount);
        Assert.Equal(expectedDecimalAmount, actualMoneyWithDouble.Amount);
    }

    [Fact]
        public void GivenTwoValidMoney_WhenCallSumOperator_ThenReturnAMoneyWithSumResult()
        {
            //given
            var firstMoney = Money.Of(10.256);
            var secondMoney = Money.Of(8.212);
            var expectedResult = 18.47m;

            //When
            var actualResult = firstMoney + secondMoney;

            //Then
            Assert.Equal(expectedResult, actualResult.Amount);
        }

        [Fact]
        public void GivenTwoValidMoney_WhenCallSubtractOperator_ThenReturnAMoneyWithSubtractionResult()
        {
            //given
            var firstMoney = Money.Of(10.256);
            var secondMoney = Money.Of(8.212);
            var expectedResult = 2.05m;

            //When
            var actualResult = firstMoney - secondMoney;

            //Then
            Assert.Equal(expectedResult, actualResult.Amount);
        }

        [Fact]
        public void GivenTwoValidMoney_WhenCallMultiplyOperator_ThenReturnAMoneyWithMultiplicationResult()
        {
            //given
            var firstMoney = Money.Of(10.256);
            var secondMoney = Money.Of(8.212);
            var expectedResult = 84.23m;

            //When
            var actualResult = firstMoney * secondMoney;

            //Then
            Assert.Equal(expectedResult, actualResult.Amount);
        }

        [Fact]
        public void GivenTwoValidMoney_WhenCallDivideOperator_ThenReturnAMoneyWithDivisionResult()
        {
            //given
            var firstMoney = Money.Of(10.256);
            var secondMoney = Money.Of(8.212);
            var expectedResult = 1.25m;

            //When
            var actualResult = firstMoney / secondMoney;

            //Then
            Assert.Equal(expectedResult, actualResult.Amount);
        }

        [Fact]
        public void GivenTwoValidMoneyWithSecondBeingEqualZero_WhenCallDivideOperator_ThenThrowDivideByZeroException()
        {
            //given
            var firstMoney = Money.Of(10.256);
            var secondMoney = Money.ZERO;
            var expectedErrorMessage = "Denominator should not be zero!";

            //When
            var actualError = Assert.Throws<DivideByZeroException>(() =>
                firstMoney / secondMoney
            );
            //Then
            Assert.Equal(expectedErrorMessage, actualError.Message);
        }

        [Fact]
        public void GivenTwoValidMoneyWithSameValues_WhenCallEqualsMethod_ThenReturnTrue()
        {
            // Given
            var firstMoney = Money.Of(10.5);
            var secondMoney = Money.Of(10.5);

            // When
            var result = firstMoney.Equals(secondMoney);
        
            // Then
            Assert.True(result);
        }

        [Fact]
        public void GivenTwoValidMoneyWithDifferentValues_WhenCallEqualsMethod_ThenReturnFalse()
        {
            // Given
            var firstMoney = Money.Of(10.5);
            var secondMoney = Money.Of(10.6);

            // When
            var result = firstMoney.Equals(secondMoney);
        
            // Then
            Assert.False(result);
        }
}