using TechTestPaymentApi.Domain.Entities;
using TechTestPaymentApi.Domain.Validation;

namespace TechTestPaymentApi.Tests.DomainTests
{
    public class SellerTest
    {
        [Fact]
        public void GivenValidParams_WhenCreateASeller_ThenReturnASellerCreated()
        {
            //Given
            var ExpectedName = "John";
            var ExpectedCpf = "123.456.789-00";
            var ExpectedEmail = "John@email.com";
            var ExpectedPhone = "(00)00000 0000";


            //When
            var actualSeller = new Seller(
                "John",
                "123.456.789-00",
                "John@email.com",
                "(00)00000 0000"
            );

            //Then
            Assert.NotNull(actualSeller);
            Assert.NotNull(actualSeller.Id);
            Assert.Equal(ExpectedName, actualSeller.Name);
            Assert.Equal(ExpectedCpf, actualSeller.Cpf.Value);
            Assert.Equal(ExpectedEmail, actualSeller.Email);
            Assert.Equal(ExpectedPhone, actualSeller.Phone);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void GivenAnInvalidNullOrEmptyName_WhenCreateASeller_ThenThrowException(string name)
        {
            //Given
            var expectedErrorMessage = "'Name' should not be null or empty";

            //When
            var actualException = Assert.Throws<DomainExceptionValidation>(()=>
                new Seller(
                    name,
                    "12345678900",
                    "John@email.com",
                    "(00)00000 0000"
                )
            );

            //Then
            Assert.Equal(expectedErrorMessage, actualException.Message);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void GivenAnInvalidNullOrEmptyCpf_WhenCreateASeller_ThenThrowException(string cpf)
        {
            //Given
            var expectedErrorMessage = "'Cpf' should not be null or empty";

            //When
            var actualException = Assert.Throws<DomainExceptionValidation>(() =>
                new Seller(
                    "John",
                    cpf,
                    "John@email.com",
                    "(00)00000 0000"
                )
            );
            
            //Then
            Assert.Equal(expectedErrorMessage, actualException.Message);
        }

        [Theory]
        [InlineData("12345667800")]
        [InlineData("12334567")]
        //[InlineData("123.456.789-00")]
        public void GivenAnInvalidRegexCpf_WhenCreateASeller_ThenThrowException(string cpf)
        {
            //Given
            var expectedErrorMessage = "Irregular cpf format. Expected format: 000.000.000-00";

            //When
            var actualException = Assert.Throws<DomainExceptionValidation>(() =>
                new Seller(
                    "John",
                    cpf,
                    "John@email.com",
                    "(00)00000 0000"
                )
            );
            
            //Then
            Assert.Equal(expectedErrorMessage, actualException.Message);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void GivenAnInvalidNullOrEmptyEmail_WhenCreateASeller_ThenThrowException(string email)
        {
            //Given
            var expectedErrorMessage = "'Email' should not be null or empty";

            //When
            var actualException = Assert.Throws<DomainExceptionValidation>(() =>
                new Seller(
                    "John",
                    "123.456.789-00",
                    email,
                    "(00)00000 0000"
                )
            );

            //Then
            Assert.Equal(expectedErrorMessage, actualException.Message);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void GivenAnInvalidNullOrEmptyPhone_WhenCreateASeller_ThenThrowException(string phone)
        {
            //Given
            var expectedErrorMessage = "'Phone' should not be null or empty";

            //When
            var actualException = Assert.Throws<DomainExceptionValidation>(() =>
                new Seller(
                    "John",
                    "123.456.789-00",
                    "John@email.com",
                    phone
                )
            );
            //Then
            Assert.Equal(expectedErrorMessage, actualException.Message);
        }
    
    }
}