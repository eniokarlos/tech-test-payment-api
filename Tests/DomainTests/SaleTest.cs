using TechTestPaymentApi.Domain.Entities;
using TechTestPaymentApi.Domain.Validation;
using TechTestPaymentApi.Domain.ValueObjects;
using static TechTestPaymentApi.Domain.Entities.Sale;

namespace TechTestPaymentApi.Tests.DomainTests;

public class SaleTest
{
    [Fact]
    public void GivenValidParams_WhenCreateASale_ThenReturnASaleCreated()
    {
        // Given
        var expectedSeller = new Seller(
            "John",
            "123.456.789-00",
            "jonh@gmail.com",
            "(88) 99797 9990"
        );

        var expectedSaleItems = new List<SaleItem>()
        {
            new SaleItem("Smartphone", 1200.75m, 1),
            new SaleItem("SSD-250GB", 250.00m, 1),
            new SaleItem("Pen Drive 8GB", 30.00m, 2)
        };

        var expectedStatus = Sale.Status.WAITING_PAYMENT;
        var expectedTotalPrice = Money.Of("1510.75");
        var expectedDate = DateTime.Today;
        // When

        var actualSale = new Sale(
            expectedSeller,
            expectedSaleItems
        );

        // Then
        Assert.NotNull(actualSale);
        Assert.NotNull(actualSale.Id);
        Assert.Equal(expectedSeller, actualSale.Seller);
        Assert.Equal(expectedSaleItems, actualSale.Items);
        Assert.Equal(expectedStatus, actualSale.SaleStatus);
        Assert.Equal(expectedTotalPrice, actualSale.GetTotal());
        Assert.Equal(expectedDate, actualSale.Date);
    }

    [Fact]
    public void GivenAInvalidItemsQty_WhenCreateASale_ThenThrowException()
    {
        // Given
        var expectedMessage = "Sale must have at least one item";

        // When
        var actualException = Assert.Throws<DomainExceptionValidation>(() =>
            new Sale(
                new Seller(
                    "John",
                    "123.456.789-00",
                    "jonh@gmail.com",
                    "(88) 99797 9990"
                ),
                new List<SaleItem>()
            )
        );

        // Then
        Assert.Equal(expectedMessage, actualException.Message);
    }

    [Theory]
    [InlineData(Status.SENT_TO_CARRIER)]
    [InlineData(Status.DELIVERED)]
    public void GivenAValidSaleWithWaitingPaymentStatus_WhenUpdateItToInvalidStatus_ThenThrowException(Status status)
    {
        // Given
        var actualSale = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        //When
        Assert.Equal(actualSale.SaleStatus, Status.WAITING_PAYMENT);        

        // Then
        Assert.Throws<InvalidStatusUpdateException>(() =>
            actualSale.Update(status)
        );
    }

    [Theory]
    [InlineData(Status.WAITING_PAYMENT)]
    [InlineData(Status.DELIVERED)]
    public void GivenAValidSaleWithPaymentApprovedStatus_WhenUpdateItToInvalidStatus_ThenThrowException(Status status)
    {
        // Given
        var actualSale = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        actualSale.Update(Status.PAYMENT_APPROVED);        

        //When
        Assert.Equal(actualSale.SaleStatus, Status.PAYMENT_APPROVED);

        // Then
        Assert.Throws<InvalidStatusUpdateException>(() =>
            actualSale.Update(status)
        );
    }

    [Theory]
    [InlineData(Status.CANCELED)]
    [InlineData(Status.WAITING_PAYMENT)]
    [InlineData(Status.PAYMENT_APPROVED)]
    public void GivenAValidSaleWithSentToCarrierStatus_WhenUpdateItToInvalidStatus_ThenThrowException(Status status)
    {
        // Given
        var actualSale = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        actualSale.Update(Status.PAYMENT_APPROVED);
        actualSale.Update(Status.SENT_TO_CARRIER);        

        //When
        Assert.Equal(actualSale.SaleStatus, Status.SENT_TO_CARRIER);

        // Then
        Assert.Throws<InvalidStatusUpdateException>(() =>
            actualSale.Update(status)
        );
    }

    [Theory]
    [InlineData(Status.SENT_TO_CARRIER)]
    [InlineData(Status.WAITING_PAYMENT)]
    [InlineData(Status.PAYMENT_APPROVED)]
    [InlineData(Status.DELIVERED)]
    public void GivenAValidSaleWithCanceledStatus_WhenUpdateItToInvalidStatus_ThenThrowException(Status status)
    {
        // Given
        var actualSale = new Sale(
            new Seller(
                "John",
                "123.456.789-00",
                "jonh@gmail.com",
                "(88) 99797 9990"
            ),
            new List<SaleItem>()
            {
                new SaleItem("Smartphone", 1200.75m, 1),
            }
        );

        actualSale.Update(Status.CANCELED);       

        //When
        Assert.Equal(actualSale.SaleStatus, Status.CANCELED);

        // Then
        Assert.Throws<InvalidStatusUpdateException>(() =>
            actualSale.Update(status)
        );
    }
    
}