using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TechTestPaymentApi.Application.Interfaces;
using TechTestPaymentApi.Application.Mappings;
using TechTestPaymentApi.Application.Services;
using TechTestPaymentApi.Domain.Interfaces;
using TechTestPaymentApi.Infraestructure.Repositories;

namespace TechTestPaymentApi.Ioc;

public static class DependencyInjection
{
    public static IServiceCollection AddInfraestructure(this IServiceCollection services,
    IConfiguration configuration)
    {
        services.AddScoped<ISaleRepository, SaleRepository>();
        services.AddScoped<ISaleService, SaleService>();

        services.AddAutoMapper(typeof(DomainToDtoMapping));

        return services;
    }
}