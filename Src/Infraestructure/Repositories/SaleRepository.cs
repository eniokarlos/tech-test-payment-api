using TechTestPaymentApi.Domain.Entities;
using TechTestPaymentApi.Domain.Interfaces;
using TechTestPaymentApi.Infraestructure.Context;

namespace TechTestPaymentApi.Infraestructure.Repositories;

public class SaleRepository : ISaleRepository
{
    public Sale GetById(Guid id)
    {
        return InMemoryDataBase.Sales[id];
    }

    public IEnumerable<Sale> GetSales()
    {
        return InMemoryDataBase.Sales.Values;
    }

    public Sale Register(Sale sale)
    {
        InMemoryDataBase.Sales.Add(sale.Id,sale);
        return sale;
    }

    public Sale UpdateStatus(Guid id, Sale.Status status)
    {
        InMemoryDataBase.Sales[id].Update(status);
        return InMemoryDataBase.Sales[id];
    }
}