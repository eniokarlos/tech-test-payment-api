using TechTestPaymentApi.Domain.Entities;

namespace TechTestPaymentApi.Infraestructure.Context;

public class InMemoryDataBase
{
    public static readonly Dictionary<Guid, Sale> Sales = new Dictionary<Guid, Sale>();
}