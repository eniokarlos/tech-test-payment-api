namespace TechTestPaymentApi.Domain.ValueObjects;

public sealed class Money : IValueObject
{
    public static Money ZERO = Money.Of(decimal.Zero);
    private readonly decimal _amount;


    private Money(decimal amount)
    {
        _amount = amount;
    }

    public static Money Of(decimal amount)
    {
        return new Money(Scale(amount));
    }

    public static Money Of(string amount)
    {
        amount = amount.Replace('.',',');
        return new Money(Scale(decimal.Parse(amount)));
    }

    public static Money Of(double amount)
    {
        return new Money(Scale((decimal)amount));
    }

    public bool IsGreaterThanZero()
    {
        return _amount.CompareTo(decimal.Zero) > 0;
    }

    public static bool operator > (Money a, Money b)
    {
        return a.Amount.CompareTo(b.Amount) > 0;
    }

    public static bool operator < (Money a, Money b)
    {
        return a.Amount.CompareTo(b.Amount) < 0;
    }

    public static Money operator + (Money a, Money b)
    {
    return Money.Of(decimal.Add(a.Amount, b.Amount));
    }

    public static Money operator - (Money a, Money b)
    {
        return Money.Of(decimal.Subtract(a.Amount, b.Amount));
    }

    public static Money operator * (Money a, Money b)
    {
        return Money.Of(decimal.Multiply(a.Amount, b.Amount));
    }

    public static Money operator / (Money a, Money b)
    {
        if(b.Amount == decimal.Zero)
        {
            throw new DivideByZeroException("Denominator should not be zero!");
        }
        return Money.Of(decimal.Divide(a.Amount, b.Amount));
    }

    private static decimal Scale(decimal amount)
    {
        return decimal.Round(amount, 2, MidpointRounding.ToEven);
    }


    public override bool Equals(object? obj)
    {      
        if(this == obj) return true;
        if(obj == null || GetType() != obj.GetType()) 
            return false;
        return this.Amount == ((Money)obj).Amount;
    }
    
    public override int GetHashCode()
    {
        return this.Amount.GetHashCode();
    }

    public decimal Amount
    {
        get{return _amount;}
    }
}