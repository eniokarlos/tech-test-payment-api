using System.Text.RegularExpressions;

namespace TechTestPaymentApi.Domain.ValueObjects;
public class Cpf : IValueObject
{   
    private string valueString;
    private readonly Regex pattern = new Regex(@"^\d{3}\.\d{3}\.\d{3}-\d{2}$");


    private Cpf(string value)
    {
        this.valueString = value;
    }
    
    public static Cpf Of(string value)
    {
        return new Cpf(value);
    }

    public bool IsValid()
    {
        return pattern.IsMatch(valueString);
    }

    public bool IsNullOrEmpty()
    {
        return string.IsNullOrEmpty(valueString);
    }

    public string Value 
    {
        get {return this.valueString;}
        set {this.valueString = value;}
    }
}