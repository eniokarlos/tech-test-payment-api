using TechTestPaymentApi.Domain.Validation;
using TechTestPaymentApi.Domain.ValueObjects;

namespace TechTestPaymentApi.Domain.Entities;

public class SaleItem : Entity
{
    public string Name { get; set; }
    public int Quantity { get; set; }
    public Money UnitPrice { get; set; }
    public SaleItem(string name, decimal unitPrice, int quantity)
    :base(Guid.NewGuid())
    {
        Name = name;
        UnitPrice = Money.Of(unitPrice);
        Quantity = quantity;
        Validate();
    }

    private void Validate()
    {
        DomainExceptionValidation.When(string.IsNullOrEmpty(Name), 
            "'Name' should not be null or empty");
            
        DomainExceptionValidation.When(Quantity <= 0, 
            "'Quantity' must be greater than zero");
        
        DomainExceptionValidation.When(!UnitPrice.IsGreaterThanZero(),
            "'Price' must be greater than zero");
    }

    public decimal GetTotalPrice()
    {
        return UnitPrice.Amount * Quantity;
    }
}