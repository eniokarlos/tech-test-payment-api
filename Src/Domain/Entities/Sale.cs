using TechTestPaymentApi.Domain.ValueObjects;
using TechTestPaymentApi.Domain.Validation;

namespace TechTestPaymentApi.Domain.Entities;

public class Sale : Entity
{
    public Seller Seller {get;set;}
    public List<SaleItem> Items {get;set;}
    public Status SaleStatus {get; private set;}
    public DateTime Date {get;init;}

    public Sale(Seller seller, List<SaleItem> items) 
    : base(Guid.NewGuid())
    {
        Seller = seller;
        Items = items;
        SaleStatus = Status.WAITING_PAYMENT;
        Date = DateTime.Today;
        Validate();
    }

    public void Validate()
    {
        DomainExceptionValidation.When(!Items.Any(), 
            "Sale must have at least one item");
    }

    public Money GetTotal()
    {
        var total = Money.ZERO;
        Items.ForEach(item =>
        total += Money.Of(item.GetTotalPrice()));
        return total;
    }

    public void Update(Status status)
    {
        switch(status)
        {
            case Status.PAYMENT_APPROVED:
                ApprovePayment();
            break;

            case Status.SENT_TO_CARRIER:
                SendToCarrier();
            break;

            case Status.CANCELED:
                CancelOrder();
            break;

            case Status.DELIVERED:
                DeliverOrder();
            break;

            default:
                throw new InvalidStatusUpdateException("Invalid Status option");
        }
    }

    private void ApprovePayment()
    {
        InvalidStatusUpdateException.When(SaleStatus != Status.WAITING_PAYMENT,
            "Sale must be in WAITING_PAYMENT status");
        
        SaleStatus = Status.PAYMENT_APPROVED;
    }

    private void CancelOrder()
    {
        InvalidStatusUpdateException.When(SaleStatus != Status.WAITING_PAYMENT 
        && SaleStatus != Status.PAYMENT_APPROVED,
            "Sale must be in WAITING_PAYMENT or APPROVED status");

        SaleStatus = Status.CANCELED;
    }

    private void SendToCarrier()
    {
        InvalidStatusUpdateException.When(SaleStatus != Status.PAYMENT_APPROVED,
            "Sale must be in APPROVED status");
        
        SaleStatus = Status.SENT_TO_CARRIER;
    }

    private void DeliverOrder()
    {

        InvalidStatusUpdateException.When(SaleStatus != Status.SENT_TO_CARRIER,
            "Sale must be in SENT_TO_CARRIER status");
        
        SaleStatus = Status.DELIVERED;
    }

    public enum Status
    {
        WAITING_PAYMENT, PAYMENT_APPROVED, CANCELED, SENT_TO_CARRIER, DELIVERED
    }
}