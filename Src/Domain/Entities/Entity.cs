namespace TechTestPaymentApi.Domain.Entities;
public abstract class Entity
{
    public Guid Id {get;set;}

    public Entity(Guid id)
    {
        Id = id;
    }
    
    public override bool Equals(object? obj)
    {
        if(this == obj) return true;
        if(obj == null || GetType() != obj.GetType())
            return false;
        return this.Id == ((Entity)obj).Id; 
    }

    public override int GetHashCode()
    {
        return this.Id.GetHashCode();
    }

}
