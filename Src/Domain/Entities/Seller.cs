using TechTestPaymentApi.Domain.Validation;
using TechTestPaymentApi.Domain.ValueObjects;

namespace TechTestPaymentApi.Domain.Entities;

public class Seller : Entity
{
    public string Name {get;set;}
    public Cpf Cpf {get;set;}
    public string Email {get;set;}
    public string Phone {get;set;}

    public Seller(string name, string cpf, string email, string phone)
    :base(Guid.NewGuid())
    {
        Name = name;
        Cpf = Cpf.Of(cpf);
        Email = email;
        Phone = phone;
        Validate();
    }

    private void Validate()
    {
        DomainExceptionValidation.When(string.IsNullOrEmpty(Name), 
            "'Name' should not be null or empty");

        DomainExceptionValidation.When(Cpf.IsNullOrEmpty(), 
            "'Cpf' should not be null or empty");

        DomainExceptionValidation.When(!Cpf.IsValid(), 
            "Irregular cpf format. Expected format: 000.000.000-00");

        DomainExceptionValidation.When(string.IsNullOrEmpty(Email), 
            "'Email' should not be null or empty");

        DomainExceptionValidation.When(string.IsNullOrEmpty(Phone),
            "'Phone' should not be null or empty");
    }

}