using TechTestPaymentApi.Domain.Entities;
using static TechTestPaymentApi.Domain.Entities.Sale;

namespace TechTestPaymentApi.Domain.Interfaces;

public interface ISaleRepository
{
    Sale Register(Sale sale);
    IEnumerable<Sale> GetSales();
    Sale GetById(Guid id);
    Sale UpdateStatus(Guid id,Status status);
}