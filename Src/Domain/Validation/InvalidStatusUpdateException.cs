namespace TechTestPaymentApi.Domain.Validation;

public class InvalidStatusUpdateException : Exception
{
    public InvalidStatusUpdateException(string error)
    :base(error){}

    public static void When(bool hasError, string error)
    {
        if(hasError)
            throw new InvalidStatusUpdateException(error);
    }
}