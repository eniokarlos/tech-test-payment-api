using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Application.DTOs;
using TechTestPaymentApi.Application.Interfaces;
using TechTestPaymentApi.Domain.Validation;
using static TechTestPaymentApi.Domain.Entities.Sale;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly ISaleService _service;

        public SaleController(ISaleService saleService)
        {
            _service = saleService;
        }

        [HttpGet]
        public ActionResult<List<SaleOut>> GetSales()
        {
            var sales =_service.GetSales();

            if(sales == null) return NotFound("Sales Not Found");

            return Ok(sales);
        }

        [HttpPost]
        public ActionResult AddSale(SaleIn sale)
        {
            if(sale == null) return BadRequest("Invalid sale");
            try
            {
                _service.Register(sale);
                return Ok(sale);
            }
            catch(AutoMapperMappingException e)
            {
                return BadRequest(e.InnerException?.Message);
            }
        }


        [HttpGet("{Id}")]
        public ActionResult<SaleOut> GetById(Guid Id)
        {
            try
            {
                var result = _service.GetById(Id);
                return Ok(result);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut]
        public ActionResult<SaleOut> Update(Guid id, Status status)
        {
            try
            {
                var result = _service.UpdateStatus(id, status);
                return Ok(result);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch(InvalidStatusUpdateException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}