using AutoMapper;
using TechTestPaymentApi.Application.DTOs;
using TechTestPaymentApi.Application.Interfaces;
using TechTestPaymentApi.Domain.Entities;
using TechTestPaymentApi.Domain.Interfaces;
using static TechTestPaymentApi.Domain.Entities.Sale;

namespace TechTestPaymentApi.Application.Services;

public class SaleService : ISaleService
{
    private readonly ISaleRepository _repository;
    private readonly IMapper _mapper;

    public SaleService(ISaleRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }
    public SaleOut GetById(Guid id)
    {
        var returnedSale = _repository.GetById(id);
        return _mapper.Map<SaleOut>(returnedSale);
    }

    public List<SaleOut> GetSales()
    {
        var returnedSales = _repository.GetSales();
        return _mapper.Map<List<SaleOut>>(returnedSales);
    }

    public void Register(SaleIn sale)
    {
        var saleEntity = _mapper.Map<Sale>(sale);
        _repository.Register(saleEntity);
    }

    public SaleOut UpdateStatus(Guid id, Status status)
    {
        var returnedSale = _repository.UpdateStatus(id, status); 
        return _mapper.Map<SaleOut>(returnedSale);
    }
}