namespace TechTestPaymentApi.Application.DTOs;

public class SellerIn
{
    public string Name { get; set; }
    public string Cpf { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
}
