namespace TechTestPaymentApi.Application.DTOs;

public class SaleItemIn
{
    public string Name { get; set; }
    public int Quantity { get; set; }
    public decimal unitPrice { get; set; }
}