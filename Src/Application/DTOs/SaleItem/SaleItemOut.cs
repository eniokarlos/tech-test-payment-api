using TechTestPaymentApi.Domain.ValueObjects;

namespace TechTestPaymentApi.Application.DTOs;

public class SaleItemOut
{
    public Guid Id {get;set;}
    public string Name { get; set; }
    public int Quantity { get; set; }
    public decimal UnitPrice { get; set; }
}