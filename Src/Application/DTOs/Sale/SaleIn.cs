namespace TechTestPaymentApi.Application.DTOs;

public class SaleIn
{
    public SellerIn Seller {get;set;}
    public List<SaleItemIn> Items {get;set;}
}

