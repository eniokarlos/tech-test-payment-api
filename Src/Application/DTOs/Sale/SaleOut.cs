using static TechTestPaymentApi.Domain.Entities.Sale;

namespace TechTestPaymentApi.Application.DTOs;
public class SaleOut
{
    public Guid Id { get;set;}
    public SellerOut Seller { get; set;}
    public Status SaleStatus { get; set;}
    public List<SaleItemOut> Items { get; set;}
    public DateTime Date {get;set;}
    public decimal Total {get;set;}
}
