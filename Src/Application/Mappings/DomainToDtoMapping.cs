using AutoMapper;
using TechTestPaymentApi.Application.DTOs;
using TechTestPaymentApi.Domain.Entities;

namespace TechTestPaymentApi.Application.Mappings;

public class DomainToDtoMapping : Profile
{
    public DomainToDtoMapping()
    {
        SellerDtoToSeller();
        SaleItemDtoToSaleItem();
        SaleDtotoSale();
    }

    private void SellerDtoToSeller()
    {
        //Input
        CreateMap<SellerIn,Seller>();

        //Output
        CreateMap<Seller,SellerOut>().ForMember(src =>
            src.Cpf, map => map.MapFrom(dst => dst.Cpf.Value));
    }

    private void SaleItemDtoToSaleItem()
    {
        //Input
        CreateMap<SaleItemIn, SaleItem>();

        //OutPut
        CreateMap<SaleItem, SaleItemOut>().ForMember(src =>
            src.UnitPrice, map => map.MapFrom(dst => dst.UnitPrice.Amount));

    }

    private void SaleDtotoSale()
    {
        //Input
        CreateMap<SaleIn, Sale>().IncludeMembers(src => 
            src.Items, src => src.Seller);
        
        CreateMap<List<SaleItemIn>, Sale>();
        CreateMap<SellerIn, Sale>();

        //Output
        CreateMap<Sale, SaleOut>().IncludeMembers(src =>
            src.Items, src => src.Seller)
        .ForMember(dst => dst.Total, 
        map => map.MapFrom(src => src.GetTotal().Amount));

        CreateMap<List<SaleItem>, SaleOut>();
        CreateMap<Seller, SaleOut>();

    }
}