using TechTestPaymentApi.Application.DTOs;
using static TechTestPaymentApi.Domain.Entities.Sale;

namespace TechTestPaymentApi.Application.Interfaces;

public interface ISaleService
{
    void Register(SaleIn sale);
    SaleOut GetById(Guid id);
    List<SaleOut> GetSales();
    SaleOut UpdateStatus(Guid id, Status status);
}